package com.example.smarthome.viewModel

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.wifi.SupplicantState
import android.net.wifi.WifiManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.smarthome.Injection
import com.example.smarthome.db.KeyDao
import com.example.smarthome.model.DeletedDevices
import com.example.smarthome.model.MainRecyclerModel
import com.example.smarthome.utils.*
import com.example.smarthome.view.activities.MainActivity
import com.example.smarthome.view.adapters.MainRecyclerViewAdapter
import com.example.smarthome.view.custom.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.view.*
import org.jetbrains.anko.doAsync
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig
import java.util.*
import java.util.concurrent.TimeUnit

var fetchedList = false
const val WIFI_ACCESS_REQUEST_CODE = 11440
val mainViewModelDisposable = CompositeDisposable()


@SuppressLint("CheckResult")
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainActivityViewModel(private val activity: Activity,
                            private val recyclerView: RecyclerView,
                            private val udpManager: UdpManager): Observable() {

    private var keyDataBase: KeyDao = Injection.provideKeyDataSource(activity)
    var adapter = ObservableField<MainRecyclerViewAdapter>()
    val gridLayout = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
    var listAvailable = ObservableBoolean(false)
    private var list = mutableListOf<MainRecyclerModel>()
    private var availableListScanned = mutableListOf<MainRecyclerModel>()
    private var deletedDevicesList = mutableListOf<DeletedDevices>()

    private var deviceDetailsPacket = "48462d4131314153534953544852454144"

    init {

        fetchKeysListFromDb()
        fetchDeletedDevicesListFromDb()

        val wifiManager = activity.application.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiInfo = wifiManager.connectionInfo

        // fetch list of devices on network if user wifi connected to home wifi
        if (sharedPreferences.contains(WIFI_NAME)) {
            if (wifiInfo.supplicantState === SupplicantState.COMPLETED && sharedPreferences.getString(WIFI_NAME, "").isNotEmpty()) {
                var ssid = wifiInfo.ssid
                ssid = ssid.replace("\"", "")
                if (sharedPreferences.getString(WIFI_NAME, "") == ssid) {
                    scanDevices()
                } else {
                    Toast.makeText(activity, "شما به وای فای خانه خود متصل نیستید. لطفا به وای فای خانه خود متصل شوید تا برنامه به درستی عمل کند", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(activity, "لطفا وای فای خود را روشن  کرده و به وای فای خانه خود متصل شوید تا برنامه به درستی عمل کند", Toast.LENGTH_SHORT).show()
            }
        }

        val devicesListOnNetwork = getUdpStringObservable()?.subscribe {
            // fetch message from udp listener with rx bus
            try {
                Log.i("TAG", "udpMessage: " + it)

                    if (it.split(":")[0].contains("10.10.123")) {
                        // send ssid
                        udpManager.updBroadcastSender(hexStringToByteArray("41542b5753535349443d52696368456c657068656e744b6964730d"), "")
                        // send wifi pass
                        udpManager.updBroadcastSender(hexStringToByteArray("41542b57534b45593d5750413250534b2c4145532c6675636b796f75726963686b6964730d"), "")
                    } else {
                        val packetArr = it.split(",").toTypedArray()
                        val ip = packetArr[0].split(":")[1]
                        val code = packetArr[1]
                        var deletedDeviceFound = false
                        if (deletedDevicesList.size > 0) {
                            for (device in deletedDevicesList) {
                                if (code == device.code) {
                                    deletedDeviceFound = true
                                }
                            }
                        }

                        if (!deletedDeviceFound) {
                            if (list.size > 0) {
                                // add device to devices list on network
                                var found = false
                                if (availableListScanned.size > 0) {
                                    for (device in availableListScanned) {
                                        if (device.code == code) {
                                            found = true
                                        }
                                    }
                                    if (!found) {
                                        availableListScanned.add(MainRecyclerModel("", code))
                                    }
                                } else {
                                    availableListScanned.add(MainRecyclerModel("", code))
                                }

                                var deviceFound = false
                                for (device in list) {
                                    if (code == device.code) {
                                        updateDeviceIpOnDb(code, ip)
                                        deviceFound = true
                                    }
                                }
                                if (!deviceFound) {
                                    val newDevice = MainRecyclerModel("دستگاه جدید", code, 0, false, Color.BLACK, 0.5f, ip)
                                    addNewKeyToDb(newDevice)
                                    // todo : new devices must be ask from user for add to list
                                }
                            } else {
                                val newDevice = MainRecyclerModel("دستگاه جدید", code, 0, false, Color.BLACK, 0.5f, ip)
                                addNewKeyToDb(newDevice)
                            }
                        }
                    }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val subscribe = getMainRecyclerModel()?.subscribe {
//            var found = false
//            if (list.isNotEmpty()) {
//                for (item in list) {
//                    if (item.code == it.code) {
//                        found = true
//                    }
//                }
//            }
            if (it.name.isNotEmpty()) {
//            if (it.name.isNotEmpty() && !found) {
//                val wifiName = sharedPreferences.getString(WIFI_NAME, "")
//                val wifiPass = sharedPreferences.getString(WIFI_PASS, "")

                // send udp to fetch device ip
                udpManager.updBroadcastSender(hexStringToByteArray(deviceDetailsPacket), "")

//                addNewKeyToDb(it)
            } else {
                Toast.makeText(activity, "دستگاهی را که قصد اضافه کردن آن را دارید در لیست وجود دارد. لطفا اول ابتدا آن را حذف نمایید", Toast.LENGTH_LONG).show()
            }
        }

        val removeDeviceRx = getDecimalObservable()?.subscribe {
            if (it >= 0) {
                Toast.makeText(activity, "دستگاه مورد نظر با موفقیت حذف شد.", Toast.LENGTH_SHORT).show()
                adapter.get()?.removeItem(it)
            }
        }

        val updateDeviceName = getEditDeviceNameObservable()?.subscribe {
            if (it[1].toInt() >= 0 && it[0].isNotEmpty()) {
                Toast.makeText(activity, "نام دستگاه با موفقیت تغییر کرد", Toast.LENGTH_SHORT).show()
                adapter.get()?.editName(it)
            }
        }

        // set guide for first
        val config = ShowcaseConfig()
        config.delay = 500
        val sequence = MaterialShowcaseSequence(activity, "222")
        sequence.setConfig(config)
        sequence.addSequenceItem(recyclerView.rootView.set_home_wifi, "شما باید وای فای خانه خود را از اینجا تنظیم کنید", "متوجه شدم")
        sequence.addSequenceItem(recyclerView.rootView.add_new_key, "برای اضافه کردن دستگاه جدید بر روی این دکمه کلیک کنید", "متوجه شدم")
        sequence.addSequenceItem(recyclerView.rootView.recycleDeletedDevice, "برای بازنشاندن دستگاه های حذف شده بر روی این دکمه کلیک کنید", "متوجه شدم")
        sequence.start()


        // todo : checkNotAvailable method has a huge bug
//        checkDeviceNotAvailable()
    }

    @Suppress("NOT_A_FUNCTION_LABEL_WARNING")
    private fun scanDevices() {
        io.reactivex.Observable.interval(5, 10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .take(10)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
//                    UdpSender.updBroadcastSender("$" + "OpertScan" + ";", "")
                    doAsync {
                        udpManager.updBroadcastSender(hexStringToByteArray(deviceDetailsPacket), "")
                    }
                }
    }

    private fun checkDeviceNotAvailable() {
        io.reactivex.Observable.interval(10, 15, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.i("TAG", "checkDeviceNotAvailable: ")
                    if (availableListScanned.size > 0 && list.size > 0) {
                        for (device in list){
                            var deviceFound = false
                            for (scannedDevice in availableListScanned) {
                                if (device.code == scannedDevice.code) {
                                    deviceFound = true
                                    updateDeviceNotAvailableStatusOnDb(device.code, false)
                                    // todo : return when found
                                }
                            }
                            if (!deviceFound) {
                                updateDeviceNotAvailableStatusOnDb(device.code, true)
                            }
                        }
                        availableListScanned.removeAll(availableListScanned)
                    } else if (availableListScanned.size <= 0 && list.size > 0) {
                        for (device in list) {
                            updateDeviceNotAvailableStatusOnDb(device.code, true)
                        }
                    }
                }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun onClickAddNewKey() {
        DialogWifiList(activity)
    }

    fun onClickRecycleDeletedDevice() {
        DialogRecycleDeletedDevices(activity)
    }

    fun onClickSetHomeWifi() {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), WIFI_ACCESS_REQUEST_CODE)
        } else {
            DialogAvailableWifiList(activity)
        }
    }

    private fun fetchKeysListFromDb() {
        mainViewModelDisposable.add(fetchAllKeys()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ this.receivedKeysListFromDb(it) }, { this.onErrorReceive(it) }))
    }

    private fun fetchDeletedDevicesListFromDb() {
        mainViewModelDisposable.add(fetchDeletedDevices()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ this.receivedDeletedDevicesListFromDb(it) }, { this.onErrorReceive(it) }))
    }

    private fun updateDeviceIpOnDb(code: String, deviceIp: String) {
        mainViewModelDisposable.add(updateDeviceIp(code, deviceIp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ this.onSuccessChanged() }, { this.onErrorReceive(it) }))
    }

    private fun updateDeviceNotAvailableStatusOnDb(code: String, deviceNotAvailableStatus: Boolean) {
        mainViewModelDisposable.add(updateDeviceNotAvailableStatus(code, deviceNotAvailableStatus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ this.onSuccessChanged() }, { this.onErrorReceive(it) }))
    }

    private fun addNewKeyToDb(keyModel: MainRecyclerModel) {
        fetchedList = false
        mainViewModelDisposable.add(addKey(keyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ this.onSuccessKeyAdded() }, { this.onErrorReceive(it) }))
    }

    private fun receivedKeysListFromDb(list: MutableList<MainRecyclerModel>) {
        this.list = list
        if (list.size > 0) {
            listAvailable.set(true)
        } else {
            listAvailable.set(false)
        }
        if (!fetchedList) {
            adapter.set(MainRecyclerViewAdapter(list, activity))
            recyclerView.scheduleLayoutAnimation()
            fetchedList = true
        }
    }

    private fun receivedDeletedDevicesListFromDb(deletedDevicesList: MutableList<DeletedDevices>) {
        this.deletedDevicesList = deletedDevicesList
    }

    private fun onSuccessKeyAdded() {
        Toast.makeText(activity, "دستگاه جدید با موفقیت اضافه شد", Toast.LENGTH_SHORT).show()
        listAvailable.set(true)
    }

    private fun onSuccessChanged() {}

    private fun onErrorReceive(throwable: Throwable) {
        throwable.printStackTrace()
    }

    private fun addKey(keyModel: MainRecyclerModel): Completable {
        return keyDataBase.addNewKey(keyModel)
    }

    private fun fetchAllKeys(): Flowable<MutableList<MainRecyclerModel>> {
        return keyDataBase.fetchKeys()
    }

    private fun fetchDeletedDevices(): Flowable<MutableList<DeletedDevices>> {
        return keyDataBase.fetchDeletedDevices()
    }

    private fun updateDeviceIp(code: String, deviceIp: String): Completable {
        return keyDataBase.updateIp(code, deviceIp)
    }

    private fun updateDeviceNotAvailableStatus(code: String, deviceNotAvailableStatus: Boolean): Completable {
        return keyDataBase.updateDeviceNotAvailableStatus(code, deviceNotAvailableStatus)
    }
}