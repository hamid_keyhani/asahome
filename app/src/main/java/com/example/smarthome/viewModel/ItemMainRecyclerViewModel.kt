package com.example.smarthome.viewModel

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.example.smarthome.Injection
import com.example.smarthome.R
import com.example.smarthome.db.KeyDao
import com.example.smarthome.model.MainRecyclerModel
import com.example.smarthome.utils.TcpClient
import com.example.smarthome.utils.getStringObservable
import com.example.smarthome.utils.hexStringToByteArray
import com.example.smarthome.view.activities.AUDIO_PERMISSION_GRANTED
import com.example.smarthome.view.custom.DialogMusicMode
import com.example.smarthome.view.custom.DialogRemoveDevice
import com.example.smarthome.view.custom.DialogStripManagement
import com.zcw.togglebutton.ToggleButton
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig
import java.util.Observable

const val REQUEST_RECORD_AUDIO_PERMISSION = 201
val statusDisposable = CompositeDisposable()
val colorDisposable = CompositeDisposable()
var musicMode = false
var stripManagementMode = false

class ItemMainRecyclerViewModel(val mainItem: MainRecyclerModel,
                                private val activity: Activity,
                                private val toggle: ToggleButton,
                                private val icon: ImageButton,
                                private val musicIcon: ImageButton,
                                private val position: Int): Observable() {

    private var keyDataBase: KeyDao = Injection.provideKeyDataSource(activity)

    var name = ObservableField<String>(mainItem.name)
    var itemChecked = ObservableBoolean(false)
    var item: MainRecyclerModel = mainItem
    var deviceNotAvailable = ObservableBoolean(false)

    private lateinit var tcpClient: TcpClient

    private val stripOffPacket = "71240fa4"
    private val stripOnPacket = "71230fa3"

    init {
        doAsync {
            tcpClient = TcpClient()
            tcpClient.run(item.deviceIp)
        }
//        blue.write("$"+"SetupNetwork,OpertSmartHome,12345678,3297;")

        itemChecked.set(item.status)

        colorDisposable.add(fetchDevice(item.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ this.onDeviceReceive(it) }, { this.onError(it) } ))


        // set toggle button status for first (binding short if not work on it in xml)
        if (itemChecked.get()) {
            toggle.setToggleOn()
            if (item.type == 1) {
                icon.setColorFilter(activity.resources.getColor(R.color.itemOn))
            }
        } else {
            toggle.setToggleOff()
            icon.setColorFilter(activity.resources.getColor(android.R.color.white))
        }
//        val subscribe = getUdpStringObservable()?.subscribe {
//            // fetch message from udp listener with rx bus
//            try {
//                val packetArr = it.split(",").toTypedArray()
//                val packetType = packetArr[0]
//                if (packetType.contains("SetingColor")) {
//                    val code = packetArr[1]
//                    val red = packetArr[2].toInt()
//                    val green = packetArr[3].toInt()
//                    val blue = packetArr[4].replace(";","").toInt()
//                    val clr = Color.rgb(green, red, blue)
//
//                    if (code == item.code && item.type == 0 && itemChecked.get()) {
//                        if (clr == Color.BLACK || item.offset == 0.0f) {
//                            icon.setColorFilter(Color.WHITE)
//                        } else {
//                            icon.setColorFilter(clr,android.graphics.PorterDuff.Mode.MULTIPLY)
//                        }
//                    }
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }

        // receive audio granted from main activity
        val audioSubscribe = getStringObservable()?.subscribe {
            val dialogMusicMode: DialogMusicMode
            if (it == AUDIO_PERMISSION_GRANTED) {
                dialogMusicMode = DialogMusicMode(activity, item.name, item.code, item.id, item.deviceIp)
                dialogMusicMode.setOnDismissListener {
                    Toast.makeText(activity, "music closed", Toast.LENGTH_SHORT).show()
                }
            }
        }

        // set guide for first
        val config = ShowcaseConfig()
        config.delay = 500
        val sequence = MaterialShowcaseSequence(activity, "2715")
        sequence.setConfig(config)
        sequence.addSequenceItem(toggle, "شما می توانید با کلیک کردن بر روی این آیتم دستگاه خود را روشن و خاموش کنید", "متوجه شدم")
        sequence.addSequenceItem(musicIcon, "شما می توانید با کلیک کردن روی این آیتم از حالت موزیک برای دستگاه خود استفاده کنید", "متوجه شدم")
        sequence.addSequenceItem(musicIcon.rootView, "شما می توانید با کلیک کردن طولانی بر روی آیتم دستگاه خود را حذف یا ریست کنید", "متوجه شدم")
        sequence.start()

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun getIcon(): Drawable =
            when (item.type) {
                0 -> activity.resources.getDrawable(R.drawable.ic_bacon_strips)
                1 -> activity.resources.getDrawable(R.drawable.ic_light_off)
             else -> activity.resources.getDrawable(R.drawable.ic_bacon_strips)
        }


    fun onClickItem() {
        if (!deviceNotAvailable.get()) {
            when (item.type) {
                0 -> onStripClick()
                1 -> onToggleClickItem()
            }
        }
    }

    private fun onStripClick() {
        if (!deviceNotAvailable.get()) {
            if (itemChecked.get()) {
                stripManagementMode = true
                DialogStripManagement(activity, item.name, item.id, item.deviceIp, tcpClient)
            } else {
                itemChecked.set(true)
                updateKeyStatusOnDb(true)
                toggle.setToggleOn()
            }
        }
    }

    fun onToggleClickItem() {
        if (!deviceNotAvailable.get()) {
            // send broadcast to clients on local network
            if (itemChecked.get()) {
                itemChecked.set(false)
                icon.setColorFilter(activity.resources.getColor(android.R.color.white))
                toggle.setToggleOff()
                doAsync {
                    if (item.type == 0) {
//                        UdpSender.updBroadcastSender("$"+"SetColor," +
//                                item.code + "," +
//                                0 + "," +
//                                0 + "," +
//                                0 + ";",
//                                item.deviceIp)
                        tcpClient.sendMessage(hexStringToByteArray(stripOffPacket))
                    } else {
//                        UdpSender.updBroadcastSender("$"+"ChangeKeyStatus," + item.code +",0,OFF;", item.deviceIp)
                    }
                }
                updateKeyStatusOnDb(false)
            } else {
                itemChecked.set(true)
                updateKeyStatusOnDb(true)
                if (item.type == 1) {
                    doAsync {
//                        UdpSender.updBroadcastSender("$"+"ChangeKeyStatus," + item.code + ",0,ON;", item.deviceIp)
                    }
                    icon.setColorFilter(activity.resources.getColor(R.color.itemOn))
                }
                toggle.setToggleOn()
            }
        }
    }

    private fun fetchDevice(id: Int): Flowable<MainRecyclerModel> {
        return keyDataBase.fetchKey(id)
    }

    private fun onDeviceReceive(it: MainRecyclerModel) {
        // set color item from db

        item = it
        deviceNotAvailable.set(it.deviceNotAvailable)

        if (itemChecked.get() &&
                item.type == 0 &&
                !deviceNotAvailable.get() &&
                !musicMode &&
                !stripManagementMode) {
            if (it.color == Color.BLACK || it.offset == 0.0f) {
                icon.setColorFilter(Color.WHITE)
            } else {
                icon.setColorFilter(it.color)
            }
            doAsync {
                Log.i("TAG", "onDeviceReceive: " + it.deviceIp)
//                UdpSender.updBroadcastSender("$"+"SetColor," +
//                        it.code + "," +
//                        (Color.green(it.color) * it.offset).toInt() + "," +
//                        (Color.red(it.color) * it.offset).toInt() + "," +
//                        (Color.blue(it.color) * it.offset).toInt() + ";",
//                item.deviceIp)
                val red = Color.red(it.color)
                val green = Color.green(it.color)
                val blue = Color.blue(it.color)

                var redHex = Integer.toHexString(red)
                if (redHex.length == 1) { redHex = "0$redHex" }
                var greenHex = Integer.toHexString(green)
                if (greenHex.length == 1) { greenHex = "0$greenHex" }
                var blueHex = Integer.toHexString(blue)
                if (blueHex.length == 1) { blueHex = "0$blueHex" }

                var validPacket = "31" + redHex + greenHex + blueHex + "000f"

                var hexSum = Integer.toHexString(TcpClient.generateCheckSum(hexStringToByteArray(validPacket)).toInt())
                if (hexSum.length == 1) { hexSum = "0$hexSum" }
                validPacket += hexSum

                tcpClient.sendMessage(hexStringToByteArray(validPacket))
            }
        }
    }

    fun onLongClick(v: View): Boolean {
        DialogRemoveDevice(activity, item, position)
        return true
    }

    fun onClickMusicModeBtn() {
        if (!deviceNotAvailable.get()) {
            if (itemChecked.get()) {
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_RECORD_AUDIO_PERMISSION)
                } else {
                    musicMode = true
                    DialogMusicMode(activity, item.name, item.code, item.id, item.deviceIp)
                }
            } else {
                itemChecked.set(true)
                updateKeyStatusOnDb(true)
                toggle.setToggleOn()
            }
        }
    }

    private fun updateKeyStatusOnDb(status: Boolean) {
        statusDisposable.add(changeKeyStatus(status)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ this.onSuccessStatusChange() }, { this.onError(it) } ))
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    private fun changeKeyStatus(status: Boolean): Completable{
        return keyDataBase.updateKeyStatus(item.id, status)
    }

    private fun onSuccessStatusChange() {

    }

}