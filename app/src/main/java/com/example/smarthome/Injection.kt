package com.example.smarthome

import android.content.Context
import com.example.smarthome.db.KeyDao
import com.example.smarthome.db.keyDb

object Injection {
    fun provideKeyDataSource(context: Context): KeyDao {
        val dataBase = keyDb.getInstance(context)
        return dataBase.KeyDao()
    }
}