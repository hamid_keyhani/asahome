package com.example.smarthome.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "keys")
data class MainRecyclerModel(   @ColumnInfo(name = "name")
                                var name:String = "",
                                @ColumnInfo(name = "code")
                                val code:String = "",
                                @ColumnInfo(name = "type")
                                val type:Int = 0,
                                @ColumnInfo(name = "status")
                                val status:Boolean = false,
                                @ColumnInfo(name = "color")
                                val color:Int = 0,
                                @ColumnInfo(name = "offset")
                                val offset:Float = 0.5f,
                                @ColumnInfo(name = "deviceIp")
                                val deviceIp:String = "",
                                @ColumnInfo(name = "deviceNotAvailable")
                                val deviceNotAvailable:Boolean = false,
                                @PrimaryKey(autoGenerate = true)
                                @ColumnInfo(name = "id")
                                val id:Int = 0)