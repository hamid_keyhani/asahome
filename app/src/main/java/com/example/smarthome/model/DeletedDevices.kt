package com.example.smarthome.model


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "delete")
data class DeletedDevices(  @ColumnInfo(name = "code")
                            val code:String = "",
                            @PrimaryKey(autoGenerate = true)
                            @ColumnInfo(name = "id")
                            val id:Int = 0)