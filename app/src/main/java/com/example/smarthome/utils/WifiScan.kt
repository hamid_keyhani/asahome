package com.example.smarthome.utils

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.ScanResult
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.util.Log
import androidx.core.app.ActivityCompat
import org.jetbrains.anko.doAsync
import java.lang.reflect.Method

class WifiScan(private val activity: Activity) {

    var wifiManager: WifiManager? = null
    var results: MutableList<ScanResult> = mutableListOf()

    fun fetchWifiAvailableList() {
        wifiManager = activity.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiReceiver = WifiScanReceiver()
        activity.registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        wifiManager!!.startScan()
    }

    fun fetchWifiSsidConnected() {
        wifiManager = activity.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiReceiver = WifiSsidConnected()
        activity.registerReceiver(wifiReceiver, IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION))
    }

    inner class WifiScanReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val list = wifiManager!!.scanResults
            results.addAll(list)
            // send list with rx bus
            wifiListFetched?.onNext(list)

            for (wifi in list){
                Log.i("wifiName : ",wifi.SSID)
            }
        }
    }

    inner class WifiSsidConnected: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val wifiInfo = wifiManager?.connectionInfo
            val ssid = wifiInfo?.ssid
            if (ssid != null) {
                wifiSsidFetched?.onNext(ssid)
            }
        }
    }
}