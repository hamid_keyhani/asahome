package com.example.smarthome.utils;


import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

public class TcpClient {

    public static final String TAG = TcpClient.class.getSimpleName();
    public static final int SERVER_PORT = 5577;
    // message to send to the server
    private String mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
//    private PrintWriter mBufferOut;
    private OutputStream mBufferOut;
    // used to read messages from the server
    private BufferedReader mBufferIn;

    private Queue<byte[]> requestList = new LinkedList<>();

    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
//    public TcpClient(OnMessageReceived listener) {
//        mMessageListener = listener;
//    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(byte [] message) {
        try {
            if (mBufferOut != null) {
                Log.d(TAG, "Sending: " + message);
                requestList.add(message);
                Log.d(TAG, "requestList: " + requestList);
                mBufferOut.write(requestList.poll());
//                    mBufferOut.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;

        if (mBufferOut != null) {
//            mBufferOut.flush();
//            mBufferOut.close();
        }

        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        mServerMessage = null;
    }

    public void run(String ip) {

        mRun = true;

        try {

            InetAddress serverAddr = InetAddress.getByName(ip);

            Log.d("TCP Client", "C: Connecting...");

            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVER_PORT);

            try {

                //sends the message to the server
//                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                mBufferOut = socket.getOutputStream();

                //receives the message which the server sends back
                mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                //in this while the client listens for the messages sent by the server
                while (mRun) {

                    mServerMessage = mBufferIn.readLine();

                    if (mServerMessage != null && mMessageListener != null) {
                        //call the method messageReceived from MyActivity class
                        mMessageListener.messageReceived(mServerMessage);
                    }

                }

                Log.d("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

            } catch (Exception e) {
                Log.e("TCP", "S: Error", e);
            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }

        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }

    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the Activity
    //class at on AsyncTask doInBackground
    public interface OnMessageReceived {
        public void messageReceived(String message);
    }

    public static long generateCheckSum(byte[] data) {
        long checksum = 0;
        for (int i = 0; i < data.length; i++) {
            checksum += (data[i] & 0xFF);
        }
        return checksum & 0xFF;
    }


}
