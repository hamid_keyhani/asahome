package com.example.smarthome.utils

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.media.audiofx.NoiseSuppressor
import android.os.Process
import android.util.Log
import com.example.smarthome.R
import java.util.*
import java.util.stream.IntStream
import kotlin.math.log2
import kotlin.math.abs
import kotlin.math.sqrt

/*
 * Thread to manage live recording/playback of voice input from the device's microphone.
 */
class AudioCustom(
        /**
         * Give the thread high priority so that it's not canceled unexpectedly, and start it
         */
        context: Context
){
    private var stopped = false
    private val bitmap: Bitmap

    private var maxPitch = 0
    private var minPitch = 0
    private var maxAmplitude = 0
    private var minAmplitude = 0

    private var zarb = 0.1

    private var recorder: AudioRecord? = null


    init {
        @SuppressLint("UseCompatLoadingForDrawables") val drawable =
                context.resources.getDrawable(R.drawable.rgb2)
        bitmap = (drawable as BitmapDrawable).bitmap

        Log.i("Audio", "Running Audio Thread")

        var N = AudioRecord.getMinBufferSize(
            8000,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_PCM_16BIT
        )
        recorder = AudioRecord(
            MediaRecorder.AudioSource.MIC,
            8000,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_PCM_16BIT,
            N * 10
        )
        NoiseSuppressor.create(recorder!!.audioSessionId)
    }

    fun startRecording() {
        stopped = false
        var buffer = ShortArray(160)
        /*
         * Initialize buffer to hold continuously recorded audio data, start recording, and start
         * playback.
         */try {
            recorder?.startRecording()
            var pitchArray = mutableListOf<Int>()
            /*
             * Loops until something outside of this thread stops it.
             * Reads the data from the recorder and writes it to the audio track for playback.
             */while (!stopped) {
                recorder?.read(buffer, 0, buffer.size)

                // calculate amplitude
                val ab: Int = buffer[0].toInt() and 0xff shr 8 or buffer[1].toInt()
                val amplitude = abs(ab)

                val pitch = fetchPitch(buffer)

                // calculate max & min pitch
//                calcMaxMinPitch(pitch.toInt())

                // calculate max & min amplitude
//                calcMaxMinAmplitude(amplitude)

//                 create array of pitches
//                var found = false
//                for (n in pitchArray) {
//                    if (n == pitch.toInt()) {
//                        found = true
//                        break
//                    }
//                }

//                if (!found && pitch.toInt() > 0) {
//                    pitchArray.add(pitchArray.size, pitch.toInt())
//                    pitchArray.sort()
//                }

//
//                if (pitch > 0) {
//                    pitchArray.add(pitchArray.size, pitch.toInt())
                    Log.i(ContentValues.TAG, "pitch: ${pitch}")
//                }
//                Log.i(ContentValues.TAG, "pitchArray: ${pitchArray}")
//                Log.i(ContentValues.TAG, "sizePit: ${pitchArray.size}")

                getColorFromPic(pitch, amplitude)
//                getColorFromPic(pitch)

            }
        } catch (x: Throwable) {
            Log.w("Audio", "Error reading voice audio", x)
        } /*
         * Frees the thread's resources after the loop completes so that it can be run again
         */ finally {
            recorder?.stop()
        }
    }

    fun stopRecording() {
        stopped = true
    }
    fun releaseRecorder() {
        recorder?.release()
    }



    private fun calcMaxMinAmplitude(amplitude: Int) {
        if (amplitude > maxAmplitude) {
            maxAmplitude = amplitude
        } else if (amplitude < minAmplitude) {
            minAmplitude = amplitude
        }
//        Log.i("TAG", "MaxAmplitude: $maxAmplitude")
//        Log.i("TAG", "minAmplitude: $minAmplitude")
    }

    private fun calcMaxMinPitch(pitch: Int) {
        if (pitch > maxPitch) {
            maxPitch = pitch
        } else if (pitch < minPitch) {
            minPitch = pitch
        }
//        Log.i("TAG", "MaxPitch: $maxPitch")
//        Log.i("TAG", "minPitch: $minPitch")
    }

    /**
     * Called from outside of the thread in order to stop the recording/playback loop
     */

    private fun getColorFromPic(pitch: Double, amplitude: Int) {
        try {
            if (pitch > 70) {

//                if (zarb >= 11.50) {
//                    zarb = 0.5
//                } else {
//                    zarb += 0.5
//                }
//                var pitchTox = pitch * 10
                var pitchTox = 0.0
                if (pitch > 79 && pitch <= 85){
                    pitchTox = pitch * 0.5
                } else if (pitch > 85 && pitch <= 90){
                    pitchTox = pitch * 0.8
                } else if (pitch > 90 && pitch <= 95){
                    pitchTox = pitch * 1.1
                } else if (pitch > 95 && pitch <= 100){
                    pitchTox = pitch * 1.5
                } else if (pitch > 100 && pitch <= 105){
                    pitchTox = pitch * 2.5
                } else if (pitch > 105 && pitch <= 110){
                    pitchTox = pitch * 3.5
                } else if (pitch > 110 && pitch <= 115){
                    pitchTox = pitch * 4.5
                } else if (pitch > 115 && pitch <= 120){
                    pitchTox = pitch * 5.5
                } else if (pitch > 120 && pitch <= 125){
                    pitchTox = pitch * 6.5
                } else if (pitch > 125 && pitch <= 130){
                    pitchTox = pitch * 7.5
                } else if (pitch > 130 && pitch <= 135){
                    pitchTox = pitch * 8.5
                }else if (pitch > 135 && pitch <= 140){
                    pitchTox = pitch * 9.5
                }else if (pitch > 140 && pitch <= 145){
                    pitchTox = pitch * 10.5
                } else if (pitch > 145 && pitch <= 150){
                    pitchTox = pitch * 11.5
                }else if (pitch > 150 && pitch <= 155){
                    pitchTox = pitch * 12.5
                }else if (pitch > 155 && pitch <= 160){
                    pitchTox = pitch * 13.5
                }else if (pitch > 160 && pitch <= 165){
                    pitchTox = pitch * 14.5
                }
//                else if (pitch > 165 && pitch < 170){
//                    pitchTox = pitch * 8
//                }else if (pitch > 170 && pitch < 175){
//                    pitchTox = pitch * 8.5
//                }else if (pitch > 175 && pitch < 180){
//                    pitchTox = pitch * 9
//                }
//                val amplitudeToPercent = amplitude / 17
//                Log.i("TAG", "amplitudeToPercent: " + amplitudeToPercent)
//                Log.i("TAG", "amplitude: " + amplitude)
                val color = bitmap.getPixel(pitchTox.toInt(), 900)
                // create array of pitches
//                val index = pitchArray.indexOf(pitch.toInt())
//                val color = colors2()[(0..60).random()]

                val redValue = Color.red(color)
                val greenValue = Color.green(color)
                val blueValue = Color.blue(color)

//                var remains: Float
//
//                remains = (redValue * amplitudeToPercent) / 100
//                var red = remains + redValue
//                if(red > 255) {
//                    red = redValue.toFloat()
//                }
//                remains = (greenValue * amplitudeToPercent) / 100
//                var green = remains + greenValue
//                if(green > 255) {
//                    green = greenValue.toFloat()
//                }
//                remains = (blueValue * amplitudeToPercent) / 100
//                var blue = remains + blueValue
//                if(blue > 255) {
//                    blue = blueValue.toFloat()
//                }

                musicModeColor?.onNext(mutableListOf(redValue.toInt(), greenValue.toInt(), blueValue.toInt()))
//                musicModeColor.onNext(mutableListOf(Color.red(color), Color.green(color), Color.blue(color)))

            } else {
                musicModeColor?.onNext(mutableListOf(0, 0, 0))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun fetchPitch(buffer: ShortArray): Double {
        var re: Short
        var im: Short
        val mySpectrum = ShortArray(buffer.size / 2 - 1)
        var i = 1
        var j = 0
        while (i < buffer.size / 2) {
            re = buffer[2 * i]
            im = buffer[2 * i + 1]
            mySpectrum[j] = sqrt(re * re + im * im.toDouble()).toShort()
            i++
            j++
        }
//        return 69+12*log2((mySpectrum[20]/220).toDouble())
        return 69+12*log2((mySpectrum[20]/110).toDouble())
    }

    private fun colors(): List<Int> {
        return listOf(
                Color.rgb(130,0,0  ),
                Color.rgb(130,0,13 ),
                Color.rgb(130,0,26 ),
                Color.rgb(130,0,39 ),
                Color.rgb(130,0,52 ),
                Color.rgb(130,0,65 ),
                Color.rgb(130,0,78 ),
                Color.rgb(130,0,91 ),
                Color.rgb(130,0,104),
                Color.rgb(130,0,117),
                Color.rgb(130,0,130),
                Color.rgb(117,0,130),
                Color.rgb(104,0,130),
                Color.rgb(91 ,0,130),
                Color.rgb(78 ,0,130),
                Color.rgb(65 ,0,130),
                Color.rgb(52 ,0,130),
                Color.rgb(39 ,0,130),
                Color.rgb(26 ,0,130),
                Color.rgb(13 ,0,130),
                Color.rgb(0  ,0,130),
                Color.rgb(0,13,130),
                Color.rgb(0,26,130),
                Color.rgb(0,39,130),
                Color.rgb(0,52,130),
                Color.rgb(0,65,130),
                Color.rgb(0,78,130),
                Color.rgb(0,91,130),
                Color.rgb(0,104,130),
                Color.rgb(0,117,130),
                Color.rgb(0,130,130),
                Color.rgb(0,130,117),
                Color.rgb(0,130,104),
                Color.rgb(0,130,91),
                Color.rgb(0,130,78),
                Color.rgb(0,130,65),
                Color.rgb(0,130,52),
                Color.rgb(0,130,39),
                Color.rgb(0,130,26),
                Color.rgb(0,130,13),
                Color.rgb(0,130,0),
                Color.rgb(13 ,130,0),
                Color.rgb(26 ,130,0),
                Color.rgb(39 ,130,0),
                Color.rgb(52 ,130,0),
                Color.rgb(65 ,130,0),
                Color.rgb(78 ,130,0),
                Color.rgb(91 ,130,0),
                Color.rgb(104,130,0),
                Color.rgb(117,130,0),
                Color.rgb(130,130,0),
                Color.rgb(130,117,0),
                Color.rgb(130,104,0),
                Color.rgb(130,91 ,0),
                Color.rgb(130,78 ,0),
                Color.rgb(130,65 ,0),
                Color.rgb(130,52 ,0),
                Color.rgb(130,39 ,0),
                Color.rgb(130,26 ,0),
                Color.rgb(130,13 ,0)
        )
    }

    private fun colors1(): List<Int> {
        return listOf(
                Color.rgb(130,0,39 ),
                Color.rgb(130,0,13 ),
                Color.rgb(104,0,130),
                Color.rgb(130,0,91 ),
                Color.rgb(130,0,104),
                Color.rgb(65 ,0,130),
                Color.rgb(130,0,0  ),
                Color.rgb(130,0,65 ),
                Color.rgb(130,91 ,0),
                Color.rgb(78 ,0,130),
                Color.rgb(0,130,39),
                Color.rgb(13 ,130,0),
                Color.rgb(39 ,0,130),
                Color.rgb(130,13 ,0),
                Color.rgb(39 ,130,0),
                Color.rgb(130,0,130),
                Color.rgb(91 ,0,130),
                Color.rgb(117,0,130),
                Color.rgb(130,0,52 ),
                Color.rgb(0,26,130),
                Color.rgb(0,130,130),
                Color.rgb(0  ,0,130),
                Color.rgb(130,0,117),
                Color.rgb(0,39,130),
                Color.rgb(0,130,78),
                Color.rgb(52 ,130,0),
                Color.rgb(13 ,0,130),
                Color.rgb(26 ,0,130),
                Color.rgb(78 ,130,0),
                Color.rgb(52 ,0,130),
                Color.rgb(130,0,26 ),
                Color.rgb(0,65,130),
                Color.rgb(130,0,78 ),
                Color.rgb(0,130,91),
                Color.rgb(91 ,130,0),
                Color.rgb(65 ,130,0),
                Color.rgb(130,130,0),
                Color.rgb(130,104,0),
                Color.rgb(0,52,130),
                Color.rgb(0,130,104),
                Color.rgb(130,39 ,0),
                Color.rgb(0,130,52),
                Color.rgb(0,13,130),
                Color.rgb(130,65 ,0),
                Color.rgb(0,91,130),
                Color.rgb(130,52 ,0),
                Color.rgb(0,104,130),
                Color.rgb(0,130,26),
                Color.rgb(0,130,0),
                Color.rgb(130,117,0),
                Color.rgb(0,78,130),
                Color.rgb(130,78 ,0),
                Color.rgb(0,117,130),
                Color.rgb(0,130,13),
                Color.rgb(130,26 ,0),
                Color.rgb(0,130,65),
                Color.rgb(104,130,0),
                Color.rgb(26 ,130,0),
                Color.rgb(0,130,117),
                Color.rgb(117,130,0)
        )
    }
    private fun colors2(): List<Int> {
        return listOf(
                Color.rgb(180,0,54 ),
                Color.rgb(180,0,18 ),
                Color.rgb(144,0,180),
                Color.rgb(180,0,126 ),
                Color.rgb(180,0,144),
                Color.rgb(90 ,0,180),
                Color.rgb(180,0,0  ),
                Color.rgb(180,0,90 ),
                Color.rgb(180,126 ,0),
                Color.rgb(108 ,0,180),
                Color.rgb(0,180,54),
                Color.rgb(18 ,180,0),
                Color.rgb(54 ,0,180),
                Color.rgb(180,18 ,0),
                Color.rgb(54 ,180,0),
                Color.rgb(180,0,180),
                Color.rgb(126 ,0,180),
                Color.rgb(162,0,180),
                Color.rgb(180,0,72 ),
                Color.rgb(0,36,180),
                Color.rgb(0,180,180),
                Color.rgb(0  ,0,180),
                Color.rgb(180,0,162),
                Color.rgb(0,54,180),
                Color.rgb(0,180,108),
                Color.rgb(72 ,180,0),
                Color.rgb(18 ,0,180),
                Color.rgb(36 ,0,180),
                Color.rgb(108 ,180,0),
                Color.rgb(72 ,0,180),
                Color.rgb(180,0,36 ),
                Color.rgb(0,90,180),
                Color.rgb(180,0,108 ),
                Color.rgb(0,180,126),
                Color.rgb(126 ,180,0),
                Color.rgb(90 ,180,0),
                Color.rgb(180,180,0),
                Color.rgb(180,144,0),
                Color.rgb(0,72,180),
                Color.rgb(0,180,144),
                Color.rgb(180,54 ,0),
                Color.rgb(0,180,72),
                Color.rgb(0,18,180),
                Color.rgb(180,90 ,0),
                Color.rgb(0,126,180),
                Color.rgb(180,72 ,0),
                Color.rgb(0,144,180),
                Color.rgb(0,180,36),
                Color.rgb(0,180,0),
                Color.rgb(180,162,0),
                Color.rgb(0,108,180),
                Color.rgb(180,108 ,0),
                Color.rgb(0,162,180),
                Color.rgb(0,180,18),
                Color.rgb(180,36 ,0),
                Color.rgb(0,180,90),
                Color.rgb(144,180,0),
                Color.rgb(36 ,180,0),
                Color.rgb(0,180,162),
                Color.rgb(162,180,0)
        )
    }
}