package com.example.smarthome.utils

import android.os.StrictMode
import android.util.Log
import org.jetbrains.anko.doAsync
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.Socket

class UdpManager {

    private val TAG = "UdpManager"
    private lateinit var socket: DatagramSocket

    init {
//        if (socket == null && socket!!.isClosed) {
//            socket.broadcast = false
            socket = DatagramSocket(48899)
//        }
    }

    //    fun updBroadcastSender(message: String, deviceIp: String) {
//        // Hack Prevent crash (sending should be done using an async task)
//        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
//        StrictMode.setThreadPolicy(policy)
//        try {
//            //Open a random port to send the package
//            socket.broadcast = true
//            val sendData = message.toByteArray()
//            val sendPacket: DatagramPacket
//            if (deviceIp.isNotEmpty()) {
//                sendPacket = DatagramPacket(sendData, sendData.size, InetAddress.getByName(deviceIp), 4000)
//                println(UdpManager::class.java.name + "Broadcast packet sent to: " + InetAddress.getByName(deviceIp))
//            } else {
//                sendPacket = DatagramPacket(sendData, sendData.size, Broadcast.getBroadcastAddress(), 4000)
//                println(UdpManager::class.java.name + "Broadcast packet sent to: " + Broadcast.getBroadcastAddress())
//            }
//            socket.send(sendPacket)
//        } catch (e: IOException) {
//            Log.e(TAG, "IOException: " + e.message)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }

    fun updBroadcastSender(sendData: ByteArray, deviceIp: String) {
        // Hack Prevent crash (sending should be done using an async task)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        try {
//            if (socket == null && socket!!.isClosed) {
//            socket.broadcast = false
//                socket = DatagramSocket(48891)
//            }
            //Open a random port to send the package
//            socket.broadcast = true
            val sendPacket: DatagramPacket
            if (deviceIp.isNotEmpty()) {
                sendPacket = DatagramPacket(sendData, sendData.size, InetAddress.getByName(deviceIp), 48899)
                println(UdpManager::class.java.name + "Broadcast packet sent to: " + InetAddress.getByName(deviceIp))
            } else {
                sendPacket = DatagramPacket(sendData, sendData.size, Broadcast.getBroadcastAddress(), 48899)
                println(UdpManager::class.java.name + "Broadcast packet sent to: " + Broadcast.getBroadcastAddress())
            }
            socket!!.send(sendPacket)
        } catch (e: IOException) {
            Log.e(TAG, "IOException: " + e.message)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var shouldRestartSocketListen = true

    @Throws(Exception::class)
    private fun listenAndWaitAndThrowIntent() {
        val recvBuf = ByteArray(15000)
        //socket.setSoTimeout(1000);
        val packet = DatagramPacket(recvBuf, recvBuf.size)
        Log.e("UDP", "Waiting for UDP broadcast")
        socket!!.receive(packet)
        val senderIP = packet.address.hostAddress
        val message = String(packet.data).trim { it <= ' ' }
        // send message and sender ip to view model item class with rx bus
        udpSubject!!.onNext("$senderIP:$message")
//        socket!!.close()
    }

    fun startListenForUDPBroadcast() {
        doAsync {
            try {
                while (shouldRestartSocketListen) {
                    listenAndWaitAndThrowIntent()
                }
            } catch (e: Exception) {
                Log.i("UDP", "no longer listening for UDP broadcasts cause of error " + e.message)
            }
        }
    }

    fun stopListen() {
        shouldRestartSocketListen = false
        try {
            doAsync {
                socket!!.close()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}