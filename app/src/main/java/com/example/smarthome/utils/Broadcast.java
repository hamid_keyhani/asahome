package com.example.smarthome.utils;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import com.example.smarthome.SmartHome;
import java.io.IOException;
import java.net.InetAddress;

public class Broadcast {
    static InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager) SmartHome.getAppContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }
}
