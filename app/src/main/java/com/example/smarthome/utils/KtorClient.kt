package com.example.smarthome.utils

import java.net.Socket


class KtorClient(deviceIp: String) {

    private val socket = Socket(deviceIp, 5577)

    fun send(message: ByteArray) {
        socket.getOutputStream().write(message)
    }

    fun closeSocket() {
        socket.close()
    }
}