package com.example.smarthome.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class AutoDisposable: LifecycleObserver {
    lateinit var compositeDisposables: CompositeDisposable

    /**
     * Binds this observer to the specified life cycle object
     *
     * @param lifecycle The lifecycle object to bind to
     */
    fun bindTo(lifecycle: Lifecycle){
        lifecycle.addObserver(this)
        compositeDisposables = CompositeDisposable()
    }

    /**
     * Registers a disposable that will be disposed when the bound lifecycle object is destroyed
     *
     * @param disposable The disposable to register
     */
    fun addDisposable(disposable: Disposable){

        if(::compositeDisposables.isInitialized){
            compositeDisposables.add(disposable)
        }
        else{
            throw Exception("This object must be bound to a lifecycle before registering a disposable")
        }
    }

    /**
     * Disposes all registered disposables
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy(){
        compositeDisposables.dispose()
    }
}