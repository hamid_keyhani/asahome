package com.example.smarthome.utils

import android.net.wifi.ScanResult
import com.example.smarthome.model.MainRecyclerModel
import io.reactivex.subjects.Subject

var subject: Subject<String>? = null
var editDeviceName: Subject<MutableList<String>>? = null
var udpSubject: Subject<String>? = null
var decimalSubject: Subject<Int>? = null
var musicModeColor: Subject<MutableList<Int>>? = null
var wifiListFetched: Subject<MutableList<ScanResult>>? = null
var wifiSsidFetched: Subject<String>? = null
var mainRecyclerModel: Subject<MainRecyclerModel>? = null

fun getStringObservable(): io.reactivex.Observable<String>? {
    return subject
}

fun getEditDeviceNameObservable(): io.reactivex.Observable<MutableList<String>>? {
    return editDeviceName
}

fun getUdpStringObservable(): io.reactivex.Observable<String>? {
    return udpSubject
}

fun getDecimalObservable(): io.reactivex.Observable<Int>? {
    return decimalSubject
}

fun getMusicModeColor(): io.reactivex.Observable<MutableList<Int>>? {
    return musicModeColor
}

fun getWifiListFetched(): io.reactivex.Observable<MutableList<ScanResult>>? {
    return wifiListFetched
}
fun getWifiSsidFetched(): io.reactivex.Observable<String>? {
    return wifiSsidFetched
}

fun getMainRecyclerModel(): io.reactivex.Observable<MainRecyclerModel>? {
    return mainRecyclerModel
}