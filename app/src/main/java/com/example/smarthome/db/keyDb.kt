package com.example.smarthome.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.smarthome.model.DeletedDevices
import com.example.smarthome.model.MainRecyclerModel

@Database(entities = arrayOf(MainRecyclerModel::class, DeletedDevices::class), version = 1)
abstract class keyDb: RoomDatabase() {
    abstract fun KeyDao(): KeyDao

    companion object {
        @Volatile private var INSTANCE: keyDb? = null

        fun getInstance(context: Context): keyDb =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDataBase(context).also { INSTANCE = it }
            }
        private fun buildDataBase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                keyDb::class.java, "key.db")
                .build()
    }
}