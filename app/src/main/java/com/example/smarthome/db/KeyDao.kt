package com.example.smarthome.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.smarthome.model.DeletedDevices
import com.example.smarthome.model.MainRecyclerModel
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface KeyDao {

    @Query("SELECT * FROM keys ORDER BY id DESC")
    fun fetchKeys(): Flowable<MutableList<MainRecyclerModel>>

    @Insert
    fun addNewKey(keyModel: MainRecyclerModel): Completable

    @Delete
    fun removeKey(device: MainRecyclerModel): Completable

    @Query("UPDATE keys SET status = :status WHERE id = :id")
    fun updateKeyStatus(id: Int, status: Boolean): Completable

    @Query("UPDATE keys SET color = :color WHERE id = :id")
    fun updateStripColor(id: Int, color: Int): Completable

    @Query("UPDATE keys SET `offset` = :offset WHERE id = :id")
    fun updateStripOffset(id: Int, offset: Float): Completable

    @Query("UPDATE keys SET `name` = :name WHERE id = :id")
    fun updateKeyName(id: Int, name: String): Completable

    @Query("UPDATE keys SET `deviceIp` = :ip WHERE code = :code")
    fun updateIp(code: String, ip: String): Completable

    @Query("UPDATE keys SET `deviceNotAvailable` = :notAvailableStatus WHERE code = :code")
    fun updateDeviceNotAvailableStatus(code: String, notAvailableStatus: Boolean): Completable

    @Query("SELECT * FROM keys WHERE id = :id")
    fun fetchKey(id: Int): Flowable<MainRecyclerModel>

    @Query("SELECT * FROM `delete` ORDER BY id DESC")
    fun fetchDeletedDevices(): Flowable<MutableList<DeletedDevices>>

    @Insert
    fun addNewDeletedDevice(deletedDeviceModel: DeletedDevices): Completable

    @Query("DELETE FROM `delete`")
    fun removeDeletedDevice(): Completable

}