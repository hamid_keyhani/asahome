package com.example.smarthome;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import timber.log.Timber;

public class SmartHome extends Application {

//    @Override
//    protected void attachBaseContext(Context base) {
//        MultiDex.install(this);
//    }

    public static Typeface IRANSanse;
    private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();

        SmartHome.context = getApplicationContext();

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/IRANSansMobileFN.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        Timber.plant(new Timber.DebugTree());
    }

    public static Typeface getIRANSanse(Context context) {
        if (IRANSanse == null) {
            IRANSanse = Typeface.createFromAsset(context.getAssets() , "fonts/IRANSansMobileFN.ttf");
        }
        return IRANSanse;
    }
    public static Typeface getBoldIRANSanse(Context context) {
        if (IRANSanse == null) {
            IRANSanse = Typeface.createFromAsset(context.getAssets() , "fonts/IRANSansMobileBoldFN.ttf");
        }
        return IRANSanse;
    }

    public static Context getAppContext() {
        return SmartHome.context;
    }
}
