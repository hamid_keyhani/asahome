package com.example.smarthome.view.activities

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.PowerManager.WakeLock
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.smarthome.R
import com.example.smarthome.databinding.ActivityMainBinding
import com.example.smarthome.utils.*
import com.example.smarthome.view.custom.*
import com.example.smarthome.viewModel.*
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.view.*
import org.jetbrains.anko.doAsync
import java.lang.Exception
import java.net.DatagramSocket
import java.util.*


const val AUDIO_PERMISSION_GRANTED = "AUDIO_PERMISSION_GRANTED"

// todo : force update & optional option must added
class MainActivity : AppCompatActivity(), Observer {

    private var doubleClickToExit = false
    private val udpManager = UdpManager()

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initRxBusVariables()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            setStatusColor()
        }

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val mainActivityViewModel = MainActivityViewModel(this,
                binding.root.recycler,
                udpManager)
        binding.vm = mainActivityViewModel
        mainActivityViewModel.addObserver(this)

        udpManager.shouldRestartSocketListen = true
        udpManager.startListenForUDPBroadcast()

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun setStatusColor() {
        val window: Window = this.window

        // clear FLAG_TRANSLUCENT_STATUS flag:
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

        // finally change the color
        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == WIFI_ACCESS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                DialogAvailableWifiList(this)
            } else {
                Toast.makeText(this, "برای مشاهده لیست وای فای دادن دسترسی الزامی است", Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                subject?.onNext(AUDIO_PERMISSION_GRANTED)
            } else {
                Toast.makeText(this, "برای اجرای حالت موزیک داشتن دسترسی برای ضبط صدا الزامی می باشد", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun update(p0: Observable?, p1: Any?) {}

    override fun onStart() {
        super.onStart()
//        UDPListener.shouldRestartSocketListen = true
//        UDPListener.startListenForUDPBroadcast()

    }

    override fun onResume() {
        super.onResume()
        udpManager.shouldRestartSocketListen = true
        udpManager.startListenForUDPBroadcast()
    }

    override fun onStop() {
        super.onStop()
        udpManager.stopListen()
    }


    private fun initRxBusVariables() {
        subject = PublishSubject.create()
        editDeviceName = PublishSubject.create()
        udpSubject = PublishSubject.create()
        decimalSubject = PublishSubject.create()
        musicModeColor = PublishSubject.create()
        wifiListFetched = PublishSubject.create()
        wifiSsidFetched = PublishSubject.create()
        mainRecyclerModel = PublishSubject.create()
    }

    override fun onDestroy() {
        fetchedList = false
        // clear colorDisposable disposables
        colorDisposable.clear()
        statusDisposable.clear()
        mainViewModelDisposable.clear()
        dialogRemoveDisposable.clear()
        musicDisposable.clear()
        stripDisposable.clear()
        dialogRecycleDeletedDeviceDisposable.clear()
        // set null value to all of publish subject rxjava
        subject = null
        editDeviceName = null
        udpSubject = null
        decimalSubject = null
        musicModeColor = null
        wifiListFetched = null
        wifiSsidFetched = null
        mainRecyclerModel = null
        super.onDestroy()
    }

    companion object {
        const val TAG = "MainActivity"
    }

    override fun onBackPressed() {
        if (doubleClickToExit) {
            super.onBackPressed()
            return
        }
        doubleClickToExit = true
        Toast.makeText(this, "برای خروج از برنامه یکبار دیگر کلید برگشت را بفشارید", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleClickToExit = false }, 2000)
    }

}

// todo: اگر اسم وای فای اوپرت توی لیست وای فای ها بود با لیست دیوایس ها چک کنیم اگر توی لیست بود باید ان دیوایس ریست شود