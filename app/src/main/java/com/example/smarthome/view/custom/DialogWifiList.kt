package com.example.smarthome.view.custom

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.ObservableField
import com.example.smarthome.R
import com.example.smarthome.databinding.DialogWifiListBinding
import com.example.smarthome.utils.WifiScan
import com.example.smarthome.utils.getWifiListFetched
import com.example.smarthome.utils.getWifiSsidFetched
import com.tiper.MaterialSpinner


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DialogWifiList(activity: Activity): AlertDialog(activity), MaterialSpinner.OnItemSelectedListener {

    private var wifiSsidSelected = ""
    private var builder = Builder(context)
    private val dialog: AlertDialog = builder.create()
    private var binding =
            DialogWifiListBinding.bind(LayoutInflater.from(activity).inflate(R.layout.dialog_wifi_list, null))
    var ssid = ObservableField<String>("")
    init {
        dialog.setView(binding.root)
        if (dialog.window != null){
            dialog.window.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()

        // set font on material spinner
        binding.materialSpinner.typeface = Typeface.createFromAsset(context.assets, "fonts/IRANSansMobileFN.ttf")

        binding.materialSpinner.onItemSelectedListener = this

        //fetch wifi list
        val wifiScan = WifiScan(activity)
        wifiScan.fetchWifiAvailableList()
        wifiScan.fetchWifiSsidConnected()

        binding.positiveButtonKey.setOnClickListener {
            if (wifiSsidSelected.isNotEmpty()) {
                dialog.dismiss()
              DialogAddNewDevice(activity, wifiSsidSelected)
            } else {
                Toast.makeText(context, "لطفا وای فای دستگاه مورد نظر خود را انتخاب کنید" , Toast.LENGTH_SHORT).show()
            }
        }

        binding.negativeButtonKey.setOnClickListener {
            dialog.dismiss()
        }

        val fetchedWifiList = getWifiListFetched()?.subscribe {
            var list = arrayListOf<String>()
            for (wifi in it){
                if (wifi.SSID.isNotEmpty() && wifi.SSID.contains("LED")) {
                    list.add(wifi.SSID)
                }
            }
            val adapter = ArrayAdapter<String>(context, R.layout.dropdown_menu_popup_item, list)
            binding.materialSpinner.adapter = adapter
        }

        val fetchedSsid = getWifiSsidFetched()?.subscribe {
            ssid.set(it)
        }
    }

    override fun onItemSelected(parent: MaterialSpinner, view: View?, position: Int, id: Long) {
        wifiSsidSelected = parent.editText?.text.toString()
    }

    override fun onNothingSelected(parent: MaterialSpinner) {
        // nothing to do :)
    }
}