package com.example.smarthome.view.custom

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.graphics.Typeface
import android.os.Build
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.example.smarthome.R
import com.example.smarthome.SmartHome
import com.example.smarthome.databinding.DialogAddWifiBinding
import com.example.smarthome.utils.UdpManager
import com.example.smarthome.utils.WifiScan
import com.example.smarthome.utils.getWifiListFetched
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

const val WIFI_PASS = "WIFI_PASS"
const val WIFI_NAME = "WIFI_NAME"
const val SHARED_PREF_NAME = "SHARED_PREF_OPERT1"

private var masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
val sharedPreferences = EncryptedSharedPreferences.create(
        SHARED_PREF_NAME,
        masterKeyAlias,
        SmartHome.getAppContext(),
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
)

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class DialogAvailableWifiList(activity: Activity): AlertDialog(activity) {

    private val TAG = "DialogAvailableWifiList"

    private var positiveBtnMode = "ثبت"
    private var builder = Builder(context)
    private val dialog: AlertDialog = builder.create()
    private var binding =
            DialogAddWifiBinding.bind(LayoutInflater.from(activity).inflate(R.layout.dialog_add_wifi, null))

    init {

        if (sharedPreferences.contains(WIFI_PASS) && sharedPreferences.contains(WIFI_NAME)) {
            if (sharedPreferences.getString(WIFI_PASS, "").isNotEmpty()
                    && sharedPreferences.getString(WIFI_NAME, "").isNotEmpty()) {
                positiveBtnMode = "ویرایش"
                binding.positiveButtonKey.text = positiveBtnMode
            }
        }



        val sharedPrefEditor = sharedPreferences.edit()

        // set font on material spinner
        binding.materialSpinner.typeface = Typeface.createFromAsset(context.assets, "fonts/IRANSansMobileFN.ttf")

        // set default selected wifi on box fields
        binding.wifiPassEditor.setText(sharedPreferences.getString(WIFI_PASS, ""))
        binding.materialSpinner.editText?.setText(sharedPreferences.getString(WIFI_NAME, ""))

//        binding.wifiPassEditor.text = context
        dialog.setView(binding.root)
        if (dialog.window != null){
            dialog.window.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()

        //fetch wifi list
        val wifiScan = WifiScan(activity)
        wifiScan.fetchWifiAvailableList()

        binding.positiveButtonKey.setOnClickListener {
            if (binding.materialSpinner.editText?.text.toString().isNotEmpty()) {
                sharedPrefEditor.putString(WIFI_NAME, binding.materialSpinner.editText?.text.toString())
//                sharedPrefEditor.putString(WIFI_NAME, "AndroidAP4FDB")
                sharedPrefEditor.putString(WIFI_PASS, binding.wifiPassEditor.text?.toString())
                sharedPrefEditor.apply()
                dialog.dismiss()
                Toast.makeText(context,"وای فای خانه شما با موفقیت " + positiveBtnMode + " شد", Toast.LENGTH_SHORT).show()
                // start scan devices list on network added
                scanDevices()
            } else {
                Toast.makeText(context, "لطفا موارد درخواستی را با دقت وارد کنید", Toast.LENGTH_SHORT).show()
            }
        }
        binding.negativeButtonKey.setOnClickListener {
            dialog.dismiss()
        }

        val fetchedWifiList = getWifiListFetched()?.subscribe {
            var list = arrayListOf<String>()
            for (wifi in it){
                if (wifi.SSID.isNotEmpty()) {
                    list.add(wifi.SSID)
                }
            }
            val adapter = ArrayAdapter<String>(context, R.layout.dropdown_menu_popup_item, list)
            binding.materialSpinner.adapter = adapter
        }
    }

    @SuppressLint("CheckResult")
    private fun scanDevices() {
        io.reactivex.Observable.interval(1, 1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .take(10)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
//                    UdpManager.updBroadcastSender("$" + "OpertScan" + ";", "")
                }
    }

}