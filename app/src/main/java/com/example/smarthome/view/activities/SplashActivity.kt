package com.example.smarthome.view.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.smarthome.R
import com.example.smarthome.databinding.ActivitySplashBinding
import com.example.smarthome.viewModel.SplashActivityViewModel
import java.util.Observer
import java.util.Observable

class SplashActivity : AppCompatActivity(), Observer {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.MyMaterialTheme)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivitySplashBinding>(this, R.layout.activity_splash)
        val viewModel = SplashActivityViewModel()
        binding.vm = viewModel
        viewModel.addObserver(this)


        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }, 1500)

    }

    override fun update(o: Observable?, arg: Any?) {
        TODO("Not yet implemented")
    }


}

