package com.example.smarthome.view.custom

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import com.example.smarthome.Injection
import com.example.smarthome.R
import com.example.smarthome.databinding.DialogStripColorManagementBinding
import com.example.smarthome.db.KeyDao
import com.example.smarthome.model.MainRecyclerModel
import com.example.smarthome.utils.KtorClient
import com.example.smarthome.utils.TcpClient
import com.example.smarthome.utils.hexStringToByteArray
import com.example.smarthome.viewModel.stripManagementMode
import io.ktor.util.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.jetbrains.anko.doAsync


val stripDisposable = CompositeDisposable()

@KtorExperimentalAPI
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DialogStripManagement(
    context: Context,
    title: String,
    id: Int,
    deviceIp: String,
    tcpClient: TcpClient
): AlertDialog.Builder(context) {

    private var builder = AlertDialog.Builder(context)
    private val dialog: AlertDialog = builder.create()
    private var binding =
        DialogStripColorManagementBinding.bind(
            LayoutInflater.from(context).inflate(
                R.layout.dialog_strip_color_management,
                null
            )
        )
    private var keyDataBase: KeyDao = Injection.provideKeyDataSource(context)

    private var offset = 0.5f
    private var red = 0
    private var green = 0
    private var blue = 0
    private var firstColorSet = true

    private lateinit var ktor: KtorClient

    init {
        doAsync {
            ktor = KtorClient(deviceIp)
        }

        dialog.setView(binding.root)
        if (dialog.window != null){
            dialog.window.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()

        // set device name on title
        binding.title.text = title

        stripDisposable.add(fetchStrip(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ this.onStripReceive(it) }, { this.onError(it) })
        )

        dialog.setOnDismissListener {

            stripDisposable.add(
                changeStripColor(id, Color.rgb(red, green, blue)).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe()
            )

            stripDisposable.add(
                changeStripOffset(id, offset).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe()
            )

            stripManagementMode = false

        }

        binding.stripModesLabel.setOnClickListener {

        }
        binding.colorWheel.colorChangeListener = {

            val color = Color.rgb(red, green,blue)
            val newColor = Color.rgb(Color.red(it), Color.green(it), Color.blue(it))
            // control input color for color listener bug
            if (color != newColor) {
                red = Color.red(it)
                green = Color.green(it)
                blue = Color.blue(it)

                if (!firstColorSet) {

                    var redHex = Integer.toHexString((Color.red(it) * offset).toInt())
                    if (redHex.length == 1) { redHex = "0$redHex" }
                    var greenHex = Integer.toHexString((Color.green(it) * offset).toInt())
                    if (greenHex.length == 1) { greenHex = "0$greenHex" }
                    var blueHex = Integer.toHexString((Color.blue(it) * offset).toInt())
                    if (blueHex.length == 1) { blueHex = "0$blueHex" }

                    var validPacket = "31" + redHex + greenHex + blueHex + "000f"

                    var hexSum = Integer.toHexString(TcpClient.generateCheckSum(hexStringToByteArray(validPacket)).toInt())
                    if (hexSum.length == 1) { hexSum = "0$hexSum" }
                    validPacket += hexSum


                    doAsync {
                        runBlocking {
                            delay(10)
                            ktor.send(hexStringToByteArray(validPacket))
                        }
                    }
//                    tcpClient.sendMessage(hexStringToByteArray(validPacket))
                    Log.i(TAG, "checksumOfHex: " + validPacket)

                }
            }
        }

        binding.gradientSeekBar.colorChangeListener = { o, it ->
            // control input offset for color listener bug
            if (o != offset) {
                offset = o

                if (!firstColorSet) {
//                UdpManager.updBroadcastSender(
//                    "$" + "SetColor," +
//                            code + "," +
//                            (green * o).toInt() + "," +
//                            (red * o).toInt() + "," +
//                            (blue * o).toInt() + ";", deviceIp
//                )

                    doAsync {
                        var redHex = Integer.toHexString((red * o).toInt())
                        if (redHex.length == 1) { redHex = "0$redHex" }
                        var greenHex = Integer.toHexString((green * o).toInt())
                        if (greenHex.length == 1) { greenHex = "0$greenHex" }
                        var blueHex = Integer.toHexString((blue * o).toInt())
                        if (blueHex.length == 1) { blueHex = "0$blueHex" }

                        var validPacket = "31" + redHex + greenHex + blueHex + "000f"

                        var hexSum = Integer.toHexString(TcpClient.generateCheckSum(hexStringToByteArray(validPacket)).toInt())
                        if (hexSum.length == 1) { hexSum = "0$hexSum" }
                        validPacket += hexSum

                        tcpClient.sendMessage(hexStringToByteArray(validPacket))

                        Log.i(TAG, "checksumOfHex: " + validPacket)
                    }
                }
            }
        }

//        val subscribe = getUdpStringObservable().subscribe {
//            // fetch message from udp listener with rx bus
//            Log.i(ContentValues.TAG, "rx bus: $it")
//            val packetArr = it.split(",").toTypedArray()
//            val packetType = packetArr[0]
//            if (packetType.contains("SetColor")) {
//                val id = packetArr[1]
//                val red = packetArr[2].toInt()
//                val green = packetArr[3].toInt()
//                val blue = packetArr[4].replace(";","").toInt()
//                val color = Color.rgb(red, green, blue)
//
//                if (id == code) {
//                    binding.colorWheel.rgb = color
//                }
//            }
//        }

    }

    private fun onError(it: Throwable?) {
        it?.printStackTrace()
    }

    private fun onStripReceive(it: MainRecyclerModel) {
        if (firstColorSet) {
            // set saved color on colorWheel
            binding.colorWheel.rgb = it.color
            binding.gradientSeekBar.offset = it.offset
            offset = it.offset
            firstColorSet = !firstColorSet
        }
    }

    private fun changeStripColor(id: Int, color: Int): Completable {
        return keyDataBase.updateStripColor(id, color)
    }

    private fun changeStripOffset(id: Int, offset: Float): Completable {
        return keyDataBase.updateStripOffset(id, offset)
    }

    private fun fetchStrip(id: Int): Flowable<MainRecyclerModel> {
        return keyDataBase.fetchKey(id)
    }

    companion object {
        private const val TAG = "DialogStripManagement"
    }
}