package com.example.smarthome.view.custom

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.net.wifi.SupplicantState
import android.net.wifi.WifiManager
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.example.smarthome.Injection
import com.example.smarthome.R
import com.example.smarthome.databinding.DialogOptionsOfDeviceBinding
import com.example.smarthome.db.KeyDao
import com.example.smarthome.model.DeletedDevices
import com.example.smarthome.model.MainRecyclerModel
import com.example.smarthome.utils.UdpManager
import com.example.smarthome.utils.decimalSubject
import com.example.smarthome.utils.editDeviceName
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

val dialogRemoveDisposable = CompositeDisposable()

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DialogRemoveDevice(private val activity: Activity,
                         private val device: MainRecyclerModel,
                         private val position: Int) : AlertDialog.Builder(activity) {

    private var builder = AlertDialog.Builder(context)
    private val dialog: AlertDialog = builder.create()
    private var binding =
            DialogOptionsOfDeviceBinding.bind(LayoutInflater.from(context).inflate(R.layout.dialog_options_of_device, null))

    private var keyDataBase: KeyDao = Injection.provideKeyDataSource(context)

    private val wifiManager = activity.application.getSystemService(Context.WIFI_SERVICE) as WifiManager
    private val wifiInfo = wifiManager.connectionInfo

    init {
        dialog.setView(binding.root)
        if (dialog.window != null){
            dialog.window.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()

        binding.deviceName.editText!!.setText(device.name)

        binding.reset.setOnClickListener {
            if (binding.reset.text == context.getString(R.string.reset_device)) {
                binding.helper.text = context.getString(R.string.reset_device_helper)
                binding.title.text = context.getString(R.string.title_reset)
                binding.updateName.visibility = View.GONE
                binding.reset.text = context.getString(R.string.positiveButtonKey)
                binding.remove.text = context.getString(R.string.negativeButtonKey)
            } else {
                // click on positive btn
                if (binding.title.text == context.getString(R.string.update_name)) {
                    if (binding.deviceName.editText!!.text.toString().isNotEmpty()) {
                        updateKeyNameOnDb(binding.deviceName.editText!!.text.toString())
                    }
                }
                else if (binding.title.text == context.getString(R.string.title_remove)) {
                    removeKeyFromDb()
                } else {
                    checkHomeWifiConnectedAndReset()
                }
            }
        }
        binding.remove.setOnClickListener {
             if (binding.remove.text == context.getString(R.string.remove_device)) {
                 binding.helper.text = context.getString(R.string.remove_device_helper)
                 binding.title.text = context.getString(R.string.title_remove)
                 binding.updateName.visibility = View.GONE
                 binding.reset.text = context.getString(R.string.positiveButtonKey)
                 binding.remove.text = context.getString(R.string.negativeButtonKey)
             } else {
                 dialog.dismiss()
             }
        }

        binding.updateName.setOnClickListener {
            binding.title.text = context.getString(R.string.update_name)
            binding.deviceName.visibility = View.VISIBLE
            binding.helper.visibility = View.GONE
            binding.updateName.visibility = View.GONE
            binding.reset.text = context.getString(R.string.positiveButtonKey)
            binding.remove.text = context.getString(R.string.negativeButtonKey)
        }

    }

    private fun checkHomeWifiConnectedAndReset() {
        // fetch list of devices on network if user wifi connected to home wifi
        if (sharedPreferences.contains(WIFI_NAME)) {
            if (wifiInfo.supplicantState === SupplicantState.COMPLETED && sharedPreferences.getString(WIFI_NAME, "").isNotEmpty()) {
                var ssid = wifiInfo.ssid
                ssid = ssid.replace("\"", "")
                if (sharedPreferences.getString(WIFI_NAME, "") == ssid) {
                    resetDevice()
                    removeKeyFromDb()
                } else {
                    Toast.makeText(activity, "شما به وای فای خانه خود متصل نیستید. لطفا به وای فای خانه خود متصل شوید تا دستگاه به درستی ریست شود", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(activity, "لطفا وای فای خود را روشن  کرده و به وای فای خانه خود متصل شوید", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun removeKeyFromDb() {
        dialogRemoveDisposable.add(removeKey(device)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { this.onSuccessDeviceRemoved() }, { this.onError(it) }))
    }

    private fun addRemovedDeviceToDeletedDevicesDb() {
        val deletedDevice = DeletedDevices(device.code)
        dialogRemoveDisposable.add(addDeletedDevice(deletedDevice)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { this.onSuccessDeletedDeviceAddedToDb() }, { this.onError(it) }))
    }

    private fun updateKeyNameOnDb(newName: String) {
        dialogRemoveDisposable.add(updateKeyName(device.id, newName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { this.onSuccessDeviceNameUpdated(newName) }, { this.onError(it) }))
    }

    private fun onSuccessDeviceRemoved() {
        decimalSubject?.onNext(position)
        // add removed device to deleted devices db
        addRemovedDeviceToDeletedDevicesDb()
        dialog.dismiss()
    }

    private fun onSuccessDeletedDeviceAddedToDb() {}

    private fun onSuccessDeviceNameUpdated(newName: String) {
        val editNameList = mutableListOf<String>()
        editNameList.add(newName)
        editNameList.add(position.toString())
        editDeviceName?.onNext(editNameList)
        dialog.dismiss()
    }

    private fun onError(e: Throwable) {
        e.printStackTrace()
    }

    private fun removeKey(device: MainRecyclerModel): Completable {
        return keyDataBase.removeKey(device)
    }

    private fun addDeletedDevice(device: DeletedDevices): Completable {
        return keyDataBase.addNewDeletedDevice(device)
    }

    private fun updateKeyName(id: Int, name: String): Completable {
        return keyDataBase.updateKeyName(id, name)
    }

    @SuppressLint("CheckResult")
    private fun resetDevice() {
        io.reactivex.Observable.interval(500, 500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .take(5)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
//                    UdpManager.updBroadcastSender("$"+"ResetWifi," + device.code + ";", device.deviceIp)
                }
    }
}