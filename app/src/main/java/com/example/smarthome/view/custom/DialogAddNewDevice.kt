package com.example.smarthome.view.custom

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.example.smarthome.R
import com.example.smarthome.databinding.DialogAddNewBinding
import com.example.smarthome.model.MainRecyclerModel
import com.example.smarthome.utils.mainRecyclerModel

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class DialogAddNewDevice(activity: Activity, wifiSelectedSsid: String): AlertDialog(activity) {

    private var builder = Builder(context)
    private val dialog: AlertDialog = builder.create()
    private var binding =
        DialogAddNewBinding.bind(LayoutInflater.from(activity).inflate(R.layout.dialog_add_new, null))

    init {
        dialog.setView(binding.root)
        if (dialog.window != null){
            dialog.window.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()
        binding.negativeButtonKey.setOnClickListener {
            dialog.dismiss()
        }
        binding.positiveButtonKey.setOnClickListener {
            // todo: do something
            // todo: split wifiSelectedSsid for id (get id)
            val wifiName = sharedPreferences.getString(WIFI_NAME, "")
            if (binding.deviceName.editText?.text?.isNotEmpty()!!) {
                if (wifiName.isNotEmpty() &&
                        wifiSelectedSsid.isNotEmpty()) {

                    // fetch id number of device
//                    val key = wifiSelectedSsid.replace("[^0-9]".toRegex(), "")
                    // send message and sender ip to view model item class with rx bus
//                    if  (wifiSelectedSsid.contains("Strip")) {
                    if  (wifiSelectedSsid.contains("LED")) {
                        mainRecyclerModel?.onNext(MainRecyclerModel(binding.deviceName.editText?.text.toString(), "", 0, false, Color.rgb(0,0,0), 0.5f))
//                        mainRecyclerModel.onNext(MainRecyclerModel(binding.deviceName.editText?.text.toString(), "62", 0))
                    } else if (wifiSelectedSsid.contains("Key")) {
                        mainRecyclerModel?.onNext(MainRecyclerModel(binding.deviceName.editText?.text.toString(), "10", 1, false))
                    }
                    dialog.dismiss()
                } else {
                    Toast.makeText(context, "لطفا در صفحه اصلی از انتخاب وای فای خانه خود اطمینان حاصل کنید.", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context, "لطفا نام دستگاه دلخواه خود را وارد کنید", Toast.LENGTH_SHORT).show()
            }

        }
    }

    companion object {
        private const val TAG = "DialogAddNewDevice"
    }
}