package com.example.smarthome.view.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.smarthome.R
import com.example.smarthome.databinding.ItemMainRecyclerviewBinding
import com.example.smarthome.model.MainRecyclerModel
import com.example.smarthome.viewModel.ItemMainRecyclerViewModel
import kotlinx.android.synthetic.main.dialog_music_mode.view.*
import kotlinx.android.synthetic.main.item_main_recyclerview.view.*

class MainRecyclerViewAdapter(private var list: MutableList<MainRecyclerModel>,private val activity: Activity):
    RecyclerView.Adapter<MainRecyclerViewAdapter.MainRecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainRecyclerViewHolder =
            MainRecyclerViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_main_recyclerview, parent, false))

    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: MainRecyclerViewHolder, position: Int) {
        holder.view.vm = ItemMainRecyclerViewModel(
                list[position],
                activity,
                holder.view.root.toggle,
                holder.view.root.icon,
                holder.view.root.music_icon,
                position
        )
    }

    fun addNewItems(newList: MutableList<MainRecyclerModel>) {
        list.addAll(newList)
        notifyItemRangeInserted(list.size - 1, newList.size)
    }

    fun editName(editNameListAndPosition: MutableList<String>) {
        val mPosition = editNameListAndPosition[1].toInt()
        list[mPosition].name = editNameListAndPosition[0]
        notifyItemChanged(mPosition)
        notifyItemRangeChanged(mPosition, list.size)
    }

    fun removeItem(position: Int){
//        list[position].name = name
        list.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, list.size)
    }

    inner class MainRecyclerViewHolder(var view: ItemMainRecyclerviewBinding) : RecyclerView.ViewHolder(view.root)
}