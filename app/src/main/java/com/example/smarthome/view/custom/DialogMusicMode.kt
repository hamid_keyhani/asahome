@file:Suppress("CAST_NEVER_SUCCEEDS")

package com.example.smarthome.view.custom

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.example.smarthome.Injection
import com.example.smarthome.R
import com.example.smarthome.databinding.DialogMusicModeBinding
import com.example.smarthome.db.KeyDao
import com.example.smarthome.utils.AudioCustom
import com.example.smarthome.utils.UdpManager
import com.example.smarthome.utils.getMusicModeColor
import com.example.smarthome.viewModel.musicMode
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

val musicDisposable = CompositeDisposable()

@SuppressLint("UseCompatLoadingForDrawables", "InvalidWakeLockTag")
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DialogMusicMode(activity: Activity, title: String, code: String, id: Int, deviceIp: String): AlertDialog.Builder(activity) {

    private var builder = android.app.AlertDialog.Builder(activity)
    private val dialog: android.app.AlertDialog = builder.create()
    private var binding =
            DialogMusicModeBinding.bind(LayoutInflater.from(activity).inflate(R.layout.dialog_music_mode, null))

    private var audioCustom = AudioCustom(activity)

    private var keyDataBase: KeyDao = Injection.provideKeyDataSource(context)

    private var musicModeState = false

    private var red = 0
    private var green = 0
    private var blue = 0

    init {

        // keep screen on
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        dialog.setView(binding.root)
        if (dialog.window != null){
            dialog.window.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()

        binding.title.text = title
        binding.music.colorFilter = PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY)

        binding.music.setOnClickListener {

            if (!musicModeState) {
    //            val rotateAnimation = RotateAnimation(
    //                0.0f,
    //                360.0f,
    //                Animation.RELATIVE_TO_SELF,
    //                0.5f,
    //                Animation.RELATIVE_TO_SELF,
    //                0.5f
    //            )
    //            rotateAnimation.duration = 1600
    //            rotateAnimation.interpolator = LinearInterpolator()
    //            rotateAnimation.repeatCount = Animation.INFINITE
    //            binding.music.startAnimation(rotateAnimation)

                doAsync {
                    audioCustom.startRecording()
                }
                    musicModeState = !musicModeState
            }else {
    //                binding.music.clearAnimation()
                doAsync {
                    audioCustom.stopRecording()
                }
                    musicModeState = !musicModeState
                }

            }

            dialog.setOnDismissListener {

                musicDisposable.add(changeStripColor(id, Color.rgb(red, green, blue)).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe())

                doAsync {
                    audioCustom.stopRecording()
                    audioCustom.releaseRecorder()
                }

                musicMode = false
            }

            val musicMode = getMusicModeColor()?.subscribe {

                red = it[0]
                green = it[1]
                blue = it[2]

                doAsync {
                    if (red == 0 && green == 0 && blue == 0) {

                    } else {
//                        UdpManager.updBroadcastSender("$"+"SetColor," +
//                                code + "," +
//                                green + "," +
//                                red + "," +
//                                blue + ";",
//                        deviceIp)
                    }
                    uiThread {
                        if (red == 0 && green == 0 && blue == 0) {
    //                        binding.root.background.colorFilter = PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
                            binding.music.colorFilter = PorterDuffColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY)
                        } else {
    //                        binding.root.background.colorFilter = PorterDuffColorFilter(Color.rgb(red, green, blue), PorterDuff.Mode.MULTIPLY)
                            binding.music.colorFilter = PorterDuffColorFilter(Color.rgb(red, green, blue), PorterDuff.Mode.MULTIPLY)
                        }
                    }
                }
    //            binding.music.colorFilter = PorterDuffColorFilter(clr, PorterDuff.Mode.MULTIPLY)
            }
        }

        private fun changeStripColor(id: Int, color: Int): Completable {
            return keyDataBase.updateStripColor(id, color)
        }
}