package com.example.smarthome.view.custom

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.Toast
import com.example.smarthome.Injection
import com.example.smarthome.R
import com.example.smarthome.databinding.DialogRecycleDeletedDevicesBinding
import com.example.smarthome.db.KeyDao
import com.example.smarthome.utils.UdpManager
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

val dialogRecycleDeletedDeviceDisposable = CompositeDisposable()

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DialogRecycleDeletedDevices(context: Context): AlertDialog.Builder(context) {

    private var builder = AlertDialog.Builder(context)
    private val dialog: AlertDialog = builder.create()
    private var binding =
            DialogRecycleDeletedDevicesBinding.bind(LayoutInflater.from(context).inflate(R.layout.dialog_recycle_deleted_devices, null))

    private var keyDataBase: KeyDao = Injection.provideKeyDataSource(context)

    init {
        dialog.setView(binding.root)
        if (dialog.window != null){
            dialog.window.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()
        binding.negativeButtonKey.setOnClickListener {
            dialog.dismiss()
        }

        binding.positiveButtonKey.setOnClickListener {
            removeDeletedDevicesOnDb()
        }

        binding.negativeButtonKey.setOnClickListener {
            dialog.dismiss()
        }

    }

    private fun removeDeletedDevicesOnDb() {
        dialogRecycleDeletedDeviceDisposable.add(removeDeletedDevices()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { this.onSuccessDeletedDevicesRemoved() }, { this.onError(it) }))
    }

    private fun removeDeletedDevices(): Completable {
        return keyDataBase.removeDeletedDevice()
    }

    private fun onSuccessDeletedDevicesRemoved() {
        Toast.makeText(context, "عملیات بازگردانی دستگاه های حذف شده با موفقیت انجام شد. دستگاه های حذف شده بعد از چند لحظه به صورت اتوماتیک به لیست اضافه می شوند", Toast.LENGTH_LONG).show()
        dialog.dismiss()
        scanDevices()
    }

    private fun onError(e: Throwable) {
        e.printStackTrace()
    }

    @SuppressLint("CheckResult")
    private fun scanDevices() {
        io.reactivex.Observable.interval(1, 1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .take(10)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
//                    UdpManager.updBroadcastSender("$" + "OpertScan" + ";", "")
                }
    }

}